#include "pch.h"
#include <iostream>
#include <conio.h>
#include <string>
#include <time.h>

using namespace std;


int Factorial(int number)
//Given Excercise 1
{
	if (number < 0)
		//early return
	{
		cout << "Invailid number" << endl;
		return 0;
	}

	int result = 1;
	while (number > 0)
	{
		result *= number;
		number--;
	}
	return result;
}

void printArray()
{
	int n = 8;
	string items[8]{ "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };

	for (int counter = 0; counter < n; counter++)
	{
		cout << items[counter] << endl;
	}
}

void countInstances()
{
	int total = 8;
	int counted = 0;
	string currItem;
	for (int counter = 0; counter < total; counter++)
	{
		string items[]{ "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
		cout << "Item number " << counter + 1 << " " << items[counter] << endl;
		counted++;
	}
	cout << "Total Counted " << counted << endl << endl;
}

void arrayRNG()
{
	int large = 1, larger = 2, largest = 3, smallest = 20;

	srand(time(0));

	int RNG[10];

	for (int counter = 0; counter < 10; counter++)
	{
		RNG[counter] = rand() % 100 + 1;
		cout << RNG[counter] << endl;

		if (RNG[counter] >= largest)
		{
			largest = RNG[counter];
		}
		if (RNG[counter] >= larger && RNG[counter] != largest && RNG[counter] != larger)
		{
			larger = RNG[counter];
		}
		if (RNG[counter] >= large && RNG[counter] != largest && RNG[counter] != larger && RNG[counter] < larger)
		{
			large = RNG[counter];
		}
		if (RNG[counter] <= smallest)
		{
			smallest = RNG[counter];
		}
	}
	cout << "This is the Largest " << largest << endl << "This is the Larger " << larger << endl << "This is the Large " << large << endl << "This is the Smallest " << smallest << endl << endl;
}

void sortNumbersArray()
{
	int arrayB[20];
	for (int counter = 0; counter < 20; counter++)
	{
		arrayB[counter] = rand() % 1000 + 1;

		if (arrayB[counter] <= 9)
		{
			cout << arrayB[counter]<<" is a 1 digit number" << endl;
		}
		if (arrayB[counter] <= 99 && arrayB[counter] > 9)
		{
			cout << arrayB[counter] << " is a 2 digit number" << endl;
		}
		if (arrayB[counter] <= 999 && arrayB[counter] >99)
		{
			cout << arrayB[counter] << " is a 3 digit number" << endl;
		}
	}

}



int main()
{
	//Given Excercise 1
	int number;
	cout << "Input a number to be factorial";
	cin >> number;
	int result = Factorial(number);
	cout << "Result " << result << endl << endl;

	//Excercise 2
	printArray();

	//Excercise 3
	countInstances();

	//Excercise 4 and 5
	arrayRNG();
	
	//Excercise 6
	sortNumbersArray();

	return 0;
}
