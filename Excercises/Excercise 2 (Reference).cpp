#include <iostream>
#include <conio.h>
#include <string>

using namespace std;
int moneyPlayer = 1000;

void betInput(int& bet)
{
	cout << "Place your bet here" << endl;
	cin >> bet;
	if (bet > moneyPlayer)
	{
		cout << "Invaild try again" << endl;
		betInput(bet);
	}
}

void diceRoll(int& rolled, int& rollAI, int& doubles)
{
	rolled = rand() % 13;
	rollAI = rand() % 13;
	cout << "Player dice " << rolled << endl << "AI dice " << rollAI << endl;

	if (rolled == 2)
	{
		doubles = 1;
	}
}

void diceComparison(int& rolled, int& rollAI, int& category)
{
	//Win his bet
	if (rolled > rollAI)
	{
		category = 1;
	}
	//Lose his bet
	if (rolled < rollAI)
	{
		category = 2;
	}
	//bet stays salty
	if (rolled = rollAI)
	{
		category = 3;
	}
}

void betWin(int& bet, int& category, int& doubles)
{
	if (category == 1)
	{
		moneyPlayer += bet;
	}
	if (category == 2)
	{
		moneyPlayer -= bet;
	}
	if (doubles == 1)
	{
		cout << "You have rolled double eyes, you get three times your bet" << endl;
		bet *= 3;
		moneyPlayer += bet;
	}
	category = 0;
	doubles = 0;
}

int main()
{
	
	int playerBet, rolled, rollAI, doubles, bet, category;

	while (1)
	{
		if (moneyPlayer > 0)
		{
			cout << "Your current Money is " << moneyPlayer << endl;

			betInput(bet);
			diceRoll(rolled, rollAI, doubles);
			diceComparison(rolled, rollAI, category);
			betWin(bet,category,doubles);

			_getch();
		}
		else
		{
			cout << "You have lost all your money" << endl;
			break;
		}
	}
	return 0;
}