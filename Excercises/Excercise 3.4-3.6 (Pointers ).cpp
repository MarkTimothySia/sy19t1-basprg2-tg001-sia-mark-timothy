#include <iostream>
#include <string>
#include <conio.h>

using namespace std;

/*
Checklist
- Target Gold 500
- Dungeon Entrance 25, start with 50
- Loot: Mithril Ore(100), Sharp Talon(50); Thick Leather(25), Jellopy(5)
- Make separate golds, looted equivelent and gold to enter dungeon
- Player can leave anytime, make a chose action function
- Cursed Stone kills the player, loses all equivalent gold
- Bonus Multiplier +1 everytime he says yes in staying
*/

void goldChecker(int& myGold);
void enterDungeon(int& myGold, int& lootedGold);
void dungeonLooting(int& lootedGold);
void dungeonLeave(int& lootedGold, int& myGold, int& multiplier);
void goldChecker(int& myGold);

struct item
{
	string name;
	int goldWorth = 0;
}mithrilOre, jellopy, sharpTalon, thickLeather;

void gameStart(int& myGold)
{
	cout << "Reach the Target Gold of 500 and don't get a cursed stone or you will lose all your loot" << endl;
	myGold = 50;
}

void defineVariables()
{
	item mithrilOre, jellopy, sharpTalon, thickLeather;

	mithrilOre.name = "Mithril Ore ";
	jellopy.name = "Jellopy ";
	sharpTalon.name = "Sharp Talon ";
	thickLeather.name = "Thick Leather ";

	mithrilOre.goldWorth = 100;
	sharpTalon.goldWorth = 50;
	thickLeather.goldWorth = 25;
	jellopy.goldWorth = 5;
}

void enterDungeon(int& myGold, int& lootedGold)
{
	lootedGold = 0;
	if (myGold < 25)
	{
		cout << "You are out of Gold " << endl;
		exit(1);
	}

	myGold -= 25;
	cout << "You have paid 25 Gold, here is your remaining Gold " << myGold << endl;
}

void dungeonLooting(int& lootedGold)
{
	int myGold, multiplier;
	int itemFound = rand() % 4 + 1;
	cout <<"Rolled " <<itemFound << endl;

	if (itemFound == 5)
	{
		lootedGold += mithrilOre.goldWorth;
		cout << "You have found a " << mithrilOre.name << "it is worth " << mithrilOre.goldWorth << endl;
	}
	if (itemFound == 4)
	{
		lootedGold += sharpTalon.goldWorth;
		cout << "You have found a " << sharpTalon.name << "it is worth " << sharpTalon.goldWorth << endl;
	}
	if (itemFound == 3)
	{
		cout << "You have found a Curesed Stone " << endl << "You have fainted and someone found you" << endl << "You have been brought back to the village" << endl;
		lootedGold -= lootedGold;
		dungeonLeave(lootedGold,myGold,multiplier);
	}
	if (itemFound == 2)
	{
		lootedGold += thickLeather.goldWorth;
		cout << "You have found a " << thickLeather.name << "it is worth " << thickLeather.goldWorth << endl;
	}
	if (itemFound == 1)
	{
		lootedGold += jellopy.goldWorth;
		cout << "You have found a " << jellopy.name << "it is worth " << jellopy.goldWorth << endl;
	}
	system("PAUSE");

}

void dungeonLeave(int& lootedGold, int& myGold, int& multiplier)
{
	multiplier = 1;
	myGold += lootedGold;
	cout << "You have looted " << lootedGold << endl;
}

void actionLeave(int& multiplier)
{
	int input = 0;
	int lootedGold, myGold;
	cout << "Do you wish to keep on looting the dungeon? " << endl << "Type 1 is Yes, 2 is No " << endl;

	cin >> input;
	if (input == 1)
	{
		multiplier++;
		dungeonLooting(lootedGold);
	}
	if (input == 2)
	{
		dungeonLeave(lootedGold,myGold,multiplier);
	}
	if (input <= 0 || input > 2)
	{
		cout << "Input invaild try again" << endl;
	}
	system("PAUSE");

}

void goldChecker(int& myGold)
{
	if (myGold >= 500)
	{
		cout << "You have reached your Goal" << endl;
		system("PAUSE");
		exit(1);
	}
}

int main()
{
	int myGold, lootedGold;
	int multiplier = 1;
	gameStart(myGold);
	defineVariables();
	system("PAUSE");

	while (1)
	{
		enterDungeon(myGold,lootedGold);
		dungeonLooting(lootedGold);
		actionLeave(multiplier);
		goldChecker(myGold);
	}
	return 0;
}

