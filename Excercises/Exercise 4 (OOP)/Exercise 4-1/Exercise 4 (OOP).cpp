#include <iostream>
#include <string>
#include <iostream>
#include <time.h>
#include "Player.h"

using namespace std;

void printPlayer(playerStats& cloud)
{
	cout << cloud.name << "			EXP: " << cloud.exp << endl; 
	cout << "Level "<<cloud.currLvl << endl;
	cout << "HP	" << cloud.currHP << " / " << cloud.hp<<"	next level: "<<cloud.lvlNext << endl;
	cout << "MP " << cloud.currMP << " / " << cloud.mp << "	Limit level: " << cloud.lvlLimit << endl << endl;
	cout << "Strength	" << cloud.strength << endl << "Dexterity	" << cloud.dexterity << endl << "Vitality	" << cloud.vitality << endl << "Magic		" << cloud.magic << endl << "Spirit		" << cloud.spirit << endl << "Luck		" << cloud.luck << endl << endl;
	cout << "Attack 			" << cloud.atk << endl << "Attack%			" << cloud.atkPercent << endl << "Defense			" << cloud.def << endl << "Defence%		" << cloud.defPercent << endl << "Magic atk		" << cloud.magicAtk << endl << "Magic def		" << cloud.magicDef << endl << "Magic def%		" << cloud.defPercent << endl << endl;
	cout << "Weapon:		" << cloud.currWeapon << endl << "Arm:		" << cloud.currBracelet << endl << "Acc:		" << cloud.currAcc << endl;
}

void makeStats(playerStats& cloud)
{
	srand(0);
	cloud.currLvl = rand() % 99 + 1;
	// level multiplier;
	cloud.hp = cloud.hp * (cloud.currLvl / 5 + 1); // Formula not realy just emulated
	cloud.mp = cloud.mp * (cloud.currLvl / 12 + 2);
	cloud.atk = cloud.atk * (cloud.currLvl / 30 + 1);
	cloud.def = cloud.def * (cloud.currLvl / 45 + 1);

	cloud.currWeapon = "Ultima Weapon";
	cloud.currAcc = "Green Choker";
	cloud.currBracelet = "Magitek Armor";
}

int main()
{
	playerStats cloud;
	cout << "Name of player: " << endl;
	cin >> cloud.name;
	makeStats(cloud);
	printPlayer(cloud);
	return 0;
}
