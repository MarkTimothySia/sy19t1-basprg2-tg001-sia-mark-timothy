#pragma once
#include <string>
#include <cstdlib>
using namespace std;

struct playerStats
{
	//Stats
	string name;
	int hp = 140;
	int currHP =  rand()% 140;
	int mp = 23;
	int currMP = rand() % 23;
	int currLvl = 1;
	int exp = 0;
	int lvlNext = 20;
	int lvlLimit = 9999;

	//Stats in battle
	int strength = 1;
	int dexterity = 1;
	int vitality = 1;
	int magic = 1;
	int spirit = 1;
	int luck = 1;

	//Stats in output
	int atk = 15;
	int atkPercent = 1;
	int def = 12;
	int defPercent = 1;
	int magicAtk = 8;
	int magicDef = 2;
	int magicDefPercent = 1;

	//current Equipment
	string currWeapon;
	string currBracelet;
	string currAcc;

};