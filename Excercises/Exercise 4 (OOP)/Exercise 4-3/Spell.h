#pragma once
#include <string>
using namespace std;

class Wizard;

class Spell
{
public:
	Spell();
	Spell(string spellName, int damage);

private:
	string mSpellName;
	int mDamage;

};

