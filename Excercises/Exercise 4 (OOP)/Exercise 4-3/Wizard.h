#pragma once
#include <string>
using namespace std;

class Spell;
class Wizard
{
public:
	Wizard();
	Wizard(string name, int hp, int mp);

	void spellCast(string name,int damage);

private:
	string mName;
	int mHp;
	int mMp;

};
