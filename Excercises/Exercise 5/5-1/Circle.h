#pragma once
#include <string>
#include "Shapes.h"
using namespace std;

class Circle :public Shape
{
public:
	Circle();
	~Circle();

	float getArea() override;
	int setLength(int radius);

private:
	int mRadius;
};

