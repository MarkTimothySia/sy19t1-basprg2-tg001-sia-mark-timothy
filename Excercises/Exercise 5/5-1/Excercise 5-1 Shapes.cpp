#include <iostream>
#include <string>
#include <vector>
#include "Shapes.h"
#include "Square.h"
#include "Circle.h"
#include "Triangle.h"

using namespace std;

void displayeShape(Shape* shape)
{
	cout << "Name: " << shape->getName() << endl << "No. of Sides " << shape->getNumSizes() << endl << "Area: " << shape->getArea() << endl << endl;
}

int main()
{
	vector<Shape*> shapes;

	Square* square = new Square();

	square->setName("Square");
	square->setLength(10);//cin later
	square->setNumSides(4);
	shapes.push_back(square);

	Circle* circle = new Circle();

	circle->setName("Circle");
	circle->setLength(5);
	circle->setNumSides(1);
	shapes.push_back(circle);

	Triangle* triangle = new Triangle();

	triangle->setName("Triangle");
	triangle->setBase(2);
	triangle->setHeight(3);
	triangle->setNumSides(3);
	shapes.push_back(triangle);

	for (int i = 0; i < shapes.size(); i++)
	{
		Shape* shape = shapes[i];
		shape->getName();
		shape->getNumSizes();
		shape->getArea();
		displayeShape(shape);
	}
	
}
