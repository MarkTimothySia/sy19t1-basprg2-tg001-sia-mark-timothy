#include "Shapes.h"
#include <iostream>
using namespace std;

Shape::Shape()
{
	return;
}

Shape::~Shape()
{
}

int Shape::getNumSizes()
{
	return mNumSides;
}

float Shape::getArea()
{
	return 0;
}

void Shape::setName(string name)
{
	mName = name;
}

void Shape::setNumSides(int numSides)
{
	mNumSides = numSides;
}

string Shape::getName()
{
	return mName;
}
