#pragma once
#include <string>
using namespace std;

class Shape
{
public:
	Shape();
	~Shape();

	string name;
	int getNumSizes();

	virtual float getArea();
	void setName(string name);
	void setNumSides(int numSides);

	string getName();


private:
	string mName;
	int mNumSides;
};

