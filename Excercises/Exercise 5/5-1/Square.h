#pragma once
#include <string>
#include "Shapes.h"
using namespace std;

class Square : public Shape {
public:
	Square();
	~Square();

	float getArea() override;
	void setLength(int length);

private:
	int mLength;
};