#pragma once
#include <string>
#include "Shapes.h"

using namespace std;

class Triangle :public Shape
{
public:
	Triangle();
	~Triangle();

	float getArea() override;
	
	int setHeight(int height);
	int setBase(int base);

private:

	int mHeight, mBase;

};

