#pragma once
#include "Skills.h"
using namespace std;

class Skill;

struct Player
{
public:
	class Skill;

	int mHp = 10;
	int mDex = 1;
	int mPow = 1;
	int mAgi = 1;
	int mVit = 1;
};

