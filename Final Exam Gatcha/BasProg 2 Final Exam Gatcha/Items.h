#pragma once
#include <cstdlib>
#include <iostream>
#include <vector>

#include "Player.h"

using namespace std;

class Items :public Player
{
public:
	
	Items();
	~Items();


	const enum allItems
	{
		HealthPotion,
		Bomb,
		Crystals,
		R,
		SR,
		SSR
	};


private:
	string mName;
};

