#include "Player.h"

using namespace std;

Player::Player()
{
}

Player::~Player()
{
}

void Player::giveDefaultStats()
{
	int hp = 100;
	int crystals = 100;
	int points = 0;
	mHp = hp;
	mCrystals = crystals;
	mPoints = points;
}

int Player::setHp(int toAdd)
{
	return mHp += toAdd;
}

int Player::setCrystals(int toAdd)
{
	return mCrystals += toAdd;
}

int Player::setPoints(int points)
{
	return mPoints = points;
}

void Player::printStats()
{
	cout << "HP: " << mHp << endl;
	cout << "Crystals: " << mCrystals << endl;
	cout << "Points: " << mPoints << endl;
}

bool Player::alive()
{
	return mHp > 0;
}

bool Player::haveCrystals()
{
	return mCrystals > 0;
}
