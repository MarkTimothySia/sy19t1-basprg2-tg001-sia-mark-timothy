#pragma once
#include <cstdlib>
#include <iostream>


using namespace std;

class Player
{
public:
	Player();
	~Player();

	void giveDefaultStats();
	int setHp(int toAdd);
	int setCrystals(int toAdd);
	int setPoints(int points);
	void printStats();


	bool alive();
	bool haveCrystals();

private:
	int mHp;
	int mCrystals;
	int mPoints;

};

