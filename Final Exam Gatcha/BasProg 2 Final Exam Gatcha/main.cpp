#include <string>
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <vector>

#include "Player.h"
#include "Items.h"
#include "Inventory.h"

using namespace std;

const int potion = 30;
const int kaboom = -25;
const int rollCrystals = -5;
const int addCrystals = 15;

void hardRNG(Player& player, Inventory& inventory, Items& picked, int& points, int& stack);

void giveDamage(Player& player, Inventory& inventory)//Works na dont touch
{
	cout << "Current item in the vector "<<inventory.getItem() << endl;

	if (inventory.getItem() == 0) player.setHp(potion);
	else if (inventory.getItem() == 1) player.setHp(kaboom);
}

void easyRNG(Player& player, Inventory& inventory, Items& picked, int& points, int& stack)
{
	int roll = 1 + rand() % 100;
	stack = 1 + rand() % 100;
	cout << "Roll: " << roll << endl << endl;

	if (roll >= 40) //gets rare and 1 point
	{
		inventory.addItem(picked.R);
		player.setCrystals(rollCrystals);
		cout << "You have gotten a R Rarity Pull" << endl;
		points += 1;
	}
	else if (roll < 40 && roll >= 20) //gets bomb
	{
		inventory.addItem(picked.Bomb);
		cout << "You have gotten a Bomb" << endl;
		giveDamage(player,inventory);
	}
	else if (roll < 20 && roll >= 15) //gets potion or crystal after stack
	{
		if (stack > 50)
		{
			inventory.addItem(picked.Crystals);
			cout << "You have gotten 15 Crystals" << endl;
			player.setCrystals(addCrystals);
			return;
		}
		inventory.addItem(picked.HealthPotion);
		cout << "You have gotten a Potion" << endl;
		giveDamage(player, inventory);
		stack++;
	}
	else if (roll < 15 && roll >= 3) //gets Super Rare and 10 points
	{
		inventory.addItem(picked.SR);
		player.setCrystals(rollCrystals);
		cout << "You have gotten a SR Rarity Pull" << endl;
		points += 10;
	}
	else if (roll < 2 && roll <= 1) //gets SS rare and 50 points
	{
		inventory.addItem(picked.SSR);
		player.setCrystals(rollCrystals);
		cout << "You have gotten a SSR Rarity Pull" << endl;
		points += 50;
	}

	player.setPoints(points);
	system("pause");
}

void endGame(Inventory& inventory)
{
	//inventory.printInv();//tester

	cout << "You have pulled these Items" << endl << "=========================" << endl;
	inventory.checker();// shows all gotten items
}

int main() 
{
	Player player;
	Items picked;
	Inventory inventory;
	int input;
	int points = 0;
	int stack;
	srand((time(nullptr)));

	cout << "Pick which rand to use: " << endl << "1. Uses rand()% 100" << endl << "2. Uses rand()% RAND_MAX" << endl;
	cin >> input;
	player.giveDefaultStats();

	if (input == 1)
	{
		do
		{
			player.printStats();
			easyRNG(player, inventory, picked, points, stack);
			system("cls");
		} while (player.alive() && player.haveCrystals());
	}
	else if (input == 2)
	{
		do
		{
			player.printStats();
			hardRNG(player, inventory, picked, points, stack);
			system("cls");
		} while (player.alive() && player.haveCrystals());
	}
	
	endGame(inventory);
	return 0;


}

void hardRNG(Player& player, Inventory& inventory, Items& picked, int& points, int& stack)
{
	int roll = 1 + rand() % RAND_MAX;
	stack = 1 + rand() % 100;
	cout << "Roll: " << roll << endl << endl;

	if (roll >= 13106) //gets rare and 1 point
	{
		inventory.addItem(picked.R);
		player.setCrystals(rollCrystals);
		cout << "You have gotten a R Rarity Pull" << endl;
		points += 1;
	}
	else if (roll < 13106 && roll >= 6553) //gets bomb
	{
		inventory.addItem(picked.Bomb);
		cout << "You have gotten a Bomb" << endl;
		giveDamage(player, inventory);
	}
	else if (roll < 6553 && roll >= 4910) //gets potion or crystal after stack
	{
		if (stack > 50)
		{
			inventory.addItem(picked.Crystals);
			cout << "You have gotten 15 Crystals" << endl;
			player.setCrystals(addCrystals);
			return;
		}
		inventory.addItem(picked.HealthPotion);
		cout << "You have gotten a Potion" << endl;
		giveDamage(player, inventory);
		stack++;
	}
	else if (roll < 4910 && roll >= 2949) //gets Super Rare and 10 points
	{
		inventory.addItem(picked.SR);
		player.setCrystals(rollCrystals);
		cout << "You have gotten a SR Rarity Pull" << endl;
		points += 10;
	}
	else if (roll < 2949 && roll <= 328) //gets SS rare and 50 points
	{
		inventory.addItem(picked.SSR);
		player.setCrystals(rollCrystals);
		cout << "You have gotten a SSR Rarity Pull" << endl;
		points += 50;
	}

	player.setPoints(points);
	system("pause");
}

/*
- Enum Help
Vector to push all these, and have a checker to check values for last
Potion = 0,
Bomb = 1,
Crystals = 2,
R = 3,
SR = 4,
SSR = 5

Easy RNG make it 100%

Hard RNG
-5 Crystal for every pull Achieve
 - Gatcha
R = 0.4 = 1 Rare Point -> 13106
SR = 0.09 = 10 RP -> 2949
SSR = 0.01 = 50 RP -> 328

Health Potion = 0.15 = 30 Heal HP -> 4915.05
Bomb = 0.20 = -25 HP -> 6553.4
Crystal = 0.15 = 15 Crystals 2 pulls? -> 4915.05

Rand for turns -> rand if pull item or deposit for gatcha

Game Over when
Player hp = 0
Crystals = 0
*/

