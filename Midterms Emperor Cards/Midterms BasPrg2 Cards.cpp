#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <conio.h>
using namespace std;

/*
If you get King then 100 000 yen
If you get Slave then 500 000 yen

- Total Rounds 12
Goal 20 000 000 Yen
Good, Bad ,Normal Endings

- Rounds Enemy hand
1-3 , 7-9 King
4-6 , 10-12 Slave
*/

/*Checklist
- choice to pick which card to use
1mm - 10 mm bet until 30 death ending
- money bonus

*/
void cardsBattle(int& enemyCard, vector<string>& cards,int& checker,int& money, int& wager,int& life);
void endChecker(int& checker, int& winCounter, int& money, int& wager, int& life);

void startGame(int& round, int& money, int& life)
{
	life = 30;
	round = 0;
	money = 0;
	cout << "You have 12 total rounds to win 20 million Yen and a device that pierces you, 30 life point until you reach death" << endl << "You must bet at least 1mm each time to play game, it will be generated" << endl;
}

void cardInitializer(int& round, vector<string>& cards, int& enemyCard, int& money,int& life)
{
	int checker,wager;
	round++;
	cout << "Round " << round << endl;
	cout << "Player cards" << endl;

	if ((round <= 3 && round < 4 ) || (round <= 9 && round < 10 && round > 6))//Enemy King
	{
		enemyCard = 1;
		cards.push_back("Slave");
	}	
	else if ((round >= 4 && round < 7 && round > 3) || (round <= 12 && round > 9))//Enemy Slave
	{
		enemyCard = 2;
		cards.push_back("King");
	}
	
	for (int counter = 0; counter <= 3; counter++) {
		cards.push_back("Civilian");
	}
	//shows all cards
	for (int counter = 0; counter <= 4; counter++)
	{
		cout << counter + 1 << ". " << cards.at(counter) << endl;
	}

	cout << "Enemy Special: ";
	if (enemyCard == 1)
	{
		//Enemy King
		cout << "Enemy King" << endl;
	}
	else if (enemyCard == 2)
	{
		//Enemy Slave
		cout << "Enemy Slave" << endl;
	}

	cardsBattle(enemyCard, cards,checker,money,wager,life);
}

void cardsBattle(int& enemyCard, vector<string>& cards, int& checker,int& money,int& wager,int& life)
{
	srand(time(NULL));
	int winCounter;
	int choice = 0;
	int enemyChoice = rand() % 3 + 1;
	wager = rand() % 5 + 1;
	bool king{}, slave{};
	cout << "You have wagered " << wager << endl <<"Which card you wish to use?" << endl;
	cin >> choice;

	if (choice == 1)
		//King or Slave
	{
		cout << "You have choosen " << cards.at(choice-1) <<endl;
		king = cards.at(choice-1) == "King";
		slave = cards.at(choice-1) == "Slave";
	}
	else if (choice > 1)
		//Civilian
	{
		cout << "You have Choosen Default " << cards.at(choice-1) << endl;
	}

	if (enemyChoice >= 1 && enemyChoice < 3)
	{
		cout << "Enemy picked a Civilian for Battle" << endl;//tie
		if (choice > 1)
		{
			cout << "It is a tie" << endl;
			checker = 3;
		}
		else if (slave == 1)
			//Lose Slave
		{
			cout << "Your slave has been shun down by the rich civilians" << endl;
			checker = 0;
		}
		else if (king == 1)
			//Winner King
		{
			cout << "Your Larry King has won" << endl;
			money += 100000;
			checker = 1;
		}
	}
	else if (enemyChoice >= 3)
	{
		cout << "Enemy used his Special Trap Card " << endl;
		//Enemy King
		 if (enemyCard == 1)
		{
			cout << "The Forbidden King" << endl;
			if (choice > 1)
				//Lose Civilian
			{
				cout << "You have Lost to the King" << endl;
				checker = 0;
			}
			else if (slave == 1)
				//Winner Your Slave
			{
				cout << "That Slave has beaten the Burger King" << endl;
				checker = 1;
				money += 500000;
			}
		}
		 //Enemy Slave
		else if (enemyCard == 2)
		{
			cout << "The Rag Slave ";
			//Winner Civilians
			if (choice > 1)
			{
				cout << "You wake Havic towards the weak" << endl;
				checker = 1;
				money += 100000;
			}
			//Lose King
			else if (king == 1)
			{
				cout << "You weak King has seen better days" << endl;
				checker = 0;
			}
		}
	}
	endChecker(checker,winCounter,money,wager,life);
}

void endChecker(int& checker, int& winCounter, int& money, int& wager,int& life)
{
	life -= wager;
	cout << "Your current money is " << money << " , and your Life Points are " << life << endl;

	if (checker == 1)
	{
		winCounter++;
	}
	if (money == 20000000)
	{
		cout << "You have reached the target money" << endl;
	}
	if (winCounter == 12)
	{
		cout << "You have won all the matches" << endl;
	}
	if (life < 30 && life > 0)
	{
		cout << "Hey you are still alive" << endl;
	}
	else if (life < 0)
	{
		cout << "I can see that you are dead" << endl;
	}
}

void deleteCards(vector<string>& cards)
{
	cards.erase(cards.begin(), cards.end());
	system("pause");
	system("cls");

}

int main()
{
	vector<string>cards;

	int round, enemyCard,money,life;
	int winCounter = 0;
	startGame(round,money,life);
	do {
		cardInitializer(round, cards, enemyCard, money,life);
		deleteCards(cards);
	} while (round != 12);

	return 0;
}
