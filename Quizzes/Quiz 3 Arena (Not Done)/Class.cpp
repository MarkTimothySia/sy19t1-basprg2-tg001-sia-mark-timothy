#include <string>
#include <iostream>
#include "Class.h"
using namespace std;

/*
Unit::Warrior(string name, string className, int damage, int hp)
{
	name = mName;
	damage = mDamage;
	hp = mHp;
	className = mClass;
}

Unit::Mage(string name, string className, int damage, int hp)
{
	name = mName;
	damage = mDamage;
	hp = mHp;
	className = mClass;
}

Unit::Assassin(string name, string className, int damage, int hp)
{
	name = mName;
	damage = mDamage;
	hp = mHp;
	className = mClass;
}
*/

Unit::Unit()
{
	mName = "";
	mDamage = 0;
	mHp = 0;
	mClass = "";
}

Unit::Unit(string name, string className, int damage, int hp)
{
	mName = name;
	mDamage = damage;
	mHp = hp;
	mClass = className;
}

Unit::~Unit()
{
}

int Unit::setHp(int value)
{
	return mHp = value;
}

int Unit::getDamage()
{
	return mDamage;
}

int Unit::getHp()
{
	return mHp;
}

void Unit::displayStats()
{
	cout << "Player: " << mName << endl;
	cout << "HP: "<<mHp << endl;
	cout << "Damage: "<<mDamage << endl;
}

void Unit::choiceClass(string choice)
{
	choice = mClass;
}
