#pragma once
#include <string>
#include <cstdlib>
using namespace std;

class Unit
{
public:

	Unit();
	Unit(string name, string className, int damage, int hp);
	~Unit();

	/*
	Warrior(string name, string className,int damage, int hp);
	Mage(string name, string className, int damage, int hp);
	Assassin(string name, string className, int damage, int hp);
	*/

	int setHp(int value);
	int getDamage();
	int getHp();
	void displayStats();
	void choiceClass(string choice);
private:

	int mHp;
	int mDamage;
	string mClass;
	string mName;
};

