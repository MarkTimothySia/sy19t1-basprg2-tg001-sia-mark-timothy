#include <iostream>
#include <string>
#include "Class.h"

using namespace std;

/*
- Class Tier VS List _done
Stronger -> Weaker
Warrior -> Assassin -> Mage ->

+ 50% more damage to Weaker class

(Not Implemeted)
- HP reaches 0 you are dead
- POW each point +1 damage
- VIT each point -1 damage
- AGI each point decreases hit rate
- DEX each point increases the chance of hit

- Formula
damage = (POW of attacker - VIT of defender) * bonusDamage - lowest damage you can deal is 1, bonus damage is always 150%
hit% = (DEX of attacker / AGI of defender) * 100 - (use RAND)

(Implemented but useless)
- Victory Stats when class fought
Warrior = HP + 3, VIT + 3
Assassin = AGI + 3, DEX + 3
MAGE = POW + 5

- Game Rules
1 vs 1
Player wins gets healed 30% of Max HP
AGI highest attacks first
Auto battle, no decimal

- Game Flow
Get class, put name
Display Stats
Class (Monster) to fight are random, put system pause each turn
Win add new stats, Heal 30% HP
Loop Battle until either one dies
Lose Display how many enemies killed and latest stats, ends the game and program
*/
class Unit;

const int baseHp = 10;
const int baseDamage = 5;
const string classW = "Warrior";
const string classM = "Mage";
const string classA = "Assassin";
const string enemyMonster = "Enemy ";

void afterBattle(Unit* enemy,Unit* player, int& vit, int& agi, int& dex, int& pow)
{
	if (enemy->choiceClass == classW)
	{
		player->setHp = player->getHp + 3;
		vit =+ 3;
	}
	else if (enemy->choiceClass == classM)
	{
		pow = +5;
	}
	else if (enemy->choiceClass == classA) 
	{
		agi = +3;
		dex = +3;
	}

	player->setHp = player->getHp + 2;
}

void startGame( Unit* player)
{
	string name;
	string className;
	int playerNo = 0;
	cout << "Enter the name of your Character" << endl;
	cin >> name;
	cout << "Enter which class you want to take?" << endl << "1. Warrior" << endl << "2. Mage" << endl << "3. Assassin" << endl;
	cin >> playerNo;
	if (playerNo == 1) className = classW;
	else if (playerNo == 2) className = classM;
	else if (playerNo == 3) className = classA;

	Unit player(name, className, baseDamage, baseHp);

}

void defineEnemy( Unit* enemy)
{
	string className;
	int enemyNo = rand() % 3 + 1;
	if (enemyNo == 1) className = classW;
	else if (enemyNo == 2) className = classM;
	else if (enemyNo == 3) className = classA;

	Unit enemy(enemyMonster, className, baseDamage, baseHp);
}

void battle(Unit* player, Unit* enemy, int& vit, int& agi, int& dex, int& pow)
{
	do {
		if ((enemy->choiceClass == "Warrior" && player->choiceClass == "Assassin") || (enemy->choiceClass == "Assassin" && player->choiceClass == "Mage") || (enemy->choiceClass == "Mage" && player->choiceClass == "Warrior"))
		{
			player->setHp = player->getHp - (enemy->getDamage + (enemy->getDamage * (1 / 2)));
			enemy->setHp = enemy->getHp - player->getDamage;
		}
		else if ((player->choiceClass == "Warrior" && enemy->choiceClass == "Assassin") || (player->choiceClass == "Assassin" && enemy->choiceClass == "Mage") || (player->choiceClass == "Mage" && enemy->choiceClass == "Warrior"))
		{
			enemy->setHp = enemy->getHp - (player->getDamage + (player->getDamage * (1 / 2)));
			player->setHp = player->getHp - enemy->getDamage;
		}
		else
		{
			player->setHp = player->getHp - enemy->getDamage;
			enemy->setHp = enemy->getHp - player->getDamage;
		}
		
		if (player->getHp <= 0)
		{
			player->setHp = 0;
			cout << "You are Dead" << endl;
			system("pause");
			return;
		}

	} while (player->getHp != 0 || enemy->getHp > 0);


}

int main()
{
	Unit* player,* enemy;
	int vit = 0, agi = 0, dex = 0, pow = 0;
	
	startGame(player);
	player->displayStats();

	while (1)
	{
		defineEnemy(enemy);
		battle(player, enemy, vit, agi, dex, pow);
		afterBattle(enemy, player, vit, agi, dex, pow);
	}
	return 0;
}

